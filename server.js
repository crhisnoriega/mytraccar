const express = require("express");

const API_PORT = 3001;
const app = express();
const axios = require("axios");
const router = express.Router();
const bodyParser = require("body-parser");
const logger = require("morgan");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(logger("dev"));

 const url = "http://35.237.5.236/api";
//const url = "http://35.237.249.76:8082/api";

////////////////////////////////////////////////////////////
router.post("/login", async (req, res) => {
  console.log(req.body);

  try {
    var r1 = await axios.post(
      url + "/session",
      "email=dem@demo.com&password=demo",
      {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded"
        }
      }
    );

    console.log(r1);

    res.json(r1.data);
  } catch (error) {
    console.log(error.data);
    res.json(error.response.data);
  }
});

router.post("/apicall", async (req, res) => {
  console.log(req.body);
  try {
    var r1 = await axios.post(url + req.body.url, req.body.data, {
      headers: req.body.headers
    });

    console.log(r1.data);

    res.json(r1.data);
  } catch (error) {
    console.log(error);
    res.json({ error: error.response.data });
  }
});

router.post("/getapicall", async (req, res) => {
  console.log(req.body);
  try {
    var r1 = await axios.get(url + req.body.url + req.body.data, {
      headers: req.body.headers
    });

    console.log('reponse');
    console.log(r1.data);

    res.json(r1.data);
  } catch (error) {
    console.log('data');
    console.log(error.response);
    res.json({ error: error.response.data });
  }
});

app.use("/api", router);

app.listen(API_PORT, () => console.log(`API port ${API_PORT}`));
