/*!-----------------------------------------------------------------------------
 * Vegas - Fullscreen Backgrounds and Slideshows.
 * v2.4.4 - built 2018-10-30
 * Licensed under the MIT License.
 * http://vegas.jaysalvat.com/
 * ----------------------------------------------------------------------------
 * Copyright (C) 2010-2018 Jay Salvat
 * http://jaysalvat.com/
 * --------------------------------------------------------------------------*/

(function($) {
  "use strict";

  clearTimeout(window.mytimeout);
  var bb = $("body");
  var slide = bb.find("> .vegas-slide");
  slide.remove();

  $("body").removeClass("vegas-container");
  $("body")
    .find("> .vegas-slide")
    .remove();
  $("body")
    .find("> .vegas-wrapper")
    .clone(true)
    .children()
    .appendTo($("body"));
  $("body")
    .find("> .vegas-wrapper")
    .remove();    

})(window.jQuery || window.Zepto);
