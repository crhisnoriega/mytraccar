import React, { Component } from "react";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Progress,
  CardTitle,
  Button,
  CardImg,
  Badge,
  Card,
  CardBody,
  CardLink,
  CardFooter,
  CardHeader,
  Col,
  Row,
  Collapse,
  Fade,
  Input
} from "reactstrap";
import { AppSwitch } from "@coreui/react";

import Moment from "react-moment";
import axios from "axios";

import history from "../history/history";

import { connect } from "react-redux";
import { setEditInvestidor } from "../redux/redux";

class UserRowWithSelect extends Component {
  constructor(props) {
    super(props);

    this.state = {
      collapse: true,
      fadeIn: true,
      timeout: 300
    };
  }

  doEdit() {}

  render() {
    return (
      <tr>
        <td className="text-center" />
        <td>
          <Input
            type="checkbox"
            id="checkbox2"
            onClick={e => {
              this.props.callback(this.props.user);
            }}
          />
        </td>
        <td>
          <div className="medium text-muted">
            <strong>{this.props.user.name}</strong>
          </div>
        </td>

        <td>{this.props.user.email}</td>
        <td>{this.props.user.phone}</td>
        <td>{JSON.stringify(this.props.user.administrator)}</td>

        <Modal
          style={{ marginTop: "20%" }}
          isOpen={this.state.showerror}
          toggle={this.toggleInfo}
          className={"modal-danger"}
        >
          <ModalHeader toggle={this.toggleInfo}>Compra</ModalHeader>
          <ModalBody>Erro na operação</ModalBody>
          <ModalFooter>
            <Button
              color="danger"
              onClick={e => {
                this.setState({ showerror: false });
                this.props.callback();
              }}
            >
              Fechar
            </Button>{" "}
          </ModalFooter>
        </Modal>

        <Modal
          style={{ marginTop: "20%" }}
          isOpen={this.state.showok}
          toggle={this.toggleInfo}
          className={"modal-success"}
        >
          <ModalHeader toggle={this.toggleInfo}>Compra</ModalHeader>
          <ModalBody>Operação efetuada com sucesso</ModalBody>
          <ModalFooter>
            <Button
              color="success"
              onClick={e => {
                this.setState({ showok: false });
                this.props.callback();
              }}
            >
              Confirmar
            </Button>{" "}
          </ModalFooter>
        </Modal>
      </tr>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session
});

const mapDispatchToProps = {
  setEditInvestidor
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserRowWithSelect);
