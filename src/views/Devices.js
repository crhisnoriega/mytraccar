import React, { Component } from "react";

import {
  InputGroupText,
  UncontrolledDropdown,
  Container,
  Col,
  CardBody,
  Card,
  Row,
  Button,
  FormGroup,
  Label,
  Input,
  FormFeedback,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  InputGroup,
  InputGroupAddon
}
from "reactstrap";

import LaddaButton, {
  EXPAND_LEFT,
  EXPAND_RIGHT,
  EXPAND_UP,
  EXPAND_DOWN,
  CONTRACT,
  CONTRACT_OVERLAY,
  SLIDE_LEFT,
  SLIDE_RIGHT,
  SLIDE_UP,
  SLIDE_DOWN,
  ZOOM_IN,
  ZOOM_OUT
}
from "react-ladda";

import "ladda/dist/ladda-themeless.min.css";

import { Formik, Form, ErrorMessage } from "formik";
import { Effect } from "formik-effect";
import * as Yup from "yup";

import { withRouter, Link } from "react-router-dom";
import { connect } from "react-redux";
import axios from "axios";
import base64 from "react-native-base64";

import InputMask from "react-input-mask";

// loading
import Loading from "react-loading-bar";
import "react-loading-bar/dist/index.css";

// select
import Select from "react-select";
import "react-select/dist/react-select.min.css";

class Devices extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      expLeft: false,
      dropdownOpen: false,
      show: true,
      selectItem: "Selecione",
      groups: [],
      group: { name: "Selecione" },
      category: "",
      fuel: "",
      value: ["UT", "OH"],
      modelo:"",
      placa:""
    };
  }

  toggleBtn(name) {
    this.setState({
      [name]: !this.state[name],
      progress: 0.5
    });
  }

  componentDidMount() {
    this.setState({ show: true });
    axios
      .post(
        "/api/getapicall", {
          url: "/groups",
          data: "",
          headers: {
            "Content-Type": "application/json",
            Authorization: "Basic " + this.props.session.token
          }
        }, {
          headers: {
            "Content-Type": "application/json"
          }
        }
      )
      .then(res => {
        var groups1 = res.data.map(item => {
          return { value: item.id, label: item.name };
        });
        this.setState({
          groups: groups1
        });
      });
  }

  // validation
  validationSchema = function(values) {
    return Yup.object().shape({
      imei: Yup.string().required("IMEI do dispositivo obrigatorio"),
      category: Yup.string().required("categoria obrigatoria"),
      fuel: Yup.string().required("combustivel obrigatoria"),
      contact: Yup.string().required("contato obrigatorio"),
      phone: Yup.string().required("numero de telefone obrigatorio")
    });
  };

  myValidator = mySchema => {
    return values => {
      values.category = this.state.category;
      values.fuel = this.state.fuel;

      const validationSchema = mySchema(values);

      try {
        validationSchema.validateSync(values, { abortEarly: false });

        if (this.state.category == "") {
          return { category: "categoria obrigatorio" };
        }

        if (this.state.fuel == "") {
          return { fuel: "combustivel obrigatorio" };
        }

        return {};
      }
      catch (error) {
        var err = this.extractError(error);
        return err;
      }
    };
  };

  extractError = validationError => {
    const FIRST_ERROR = 0;
    return validationError.inner.reduce((errors, error) => {
      return {
        ...errors,
        [error.path]: error.errors[FIRST_ERROR]
      };
    }, {});
  };

  handleSubmit = (values, { props = this.props, setSubmitting }) => {
    values.category = this.state.category;
    values.group = null;
    
    

    var deviceinfo = {};

    deviceinfo.uniqueId = values.imei;
    deviceinfo.name = this.state.modelo + " " + this.state.placa;
    deviceinfo.groupId = this.state.group.id;
    deviceinfo.category = values.category.value;
    deviceinfo.phone = values.phone;
    deviceinfo.attributes = { plate: values.placa, fuel: values.fuel.value, operator: values.operator };
    deviceinfo.model = values.model;
    deviceinfo.contact = values.contact;

    alert(JSON.stringify(deviceinfo));

    axios
      .post(
        "/api/apicall", {
          url: "/devices",
          data: deviceinfo,
          headers: {
            "Content-Type": "application/json",
            Authorization: "Basic " + this.props.session.token
          }
        }, {
          headers: {
            "Content-Type": "application/json"
          }
        }
      )
      .then(res => {
        alert(JSON.stringify(res));
        setSubmitting(false);
      });

    return;
  };

  toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }

  selectItem = item => {
    alert(item);
    this.setState({
      selectItem: item
    });
  };

  saveChanges = value => {
    this.setState({ value });
  };

  render() {
    var categories = [
      "truck",
      "default",
      "taxi",
      "arrow",
      "pickup",
      "van",
      "animal",
      "car",
      "motorcycle",
      "offroad"
    ];

    var categories2 = categories.map(item => {
      return { value: item, label: item };
    });

    var fuels = ["Flex", "Gasolina", "Alcool", "Diesel", "GNV"];
    var fuels2 = fuels.map(item => {
      return { value: item, label: item };
    });

    return (
      <div>
        <Loading show={this.state.show} showSpinner={false} />
        <Card>
          <CardBody>
            <Formik
              initialValues={{
                name: "",

                phone: "",
                model: "",

                contact: "",
                category: "",
                group: "Selecione"
              }}
              validate={this.myValidator(this.validationSchema)}
              onSubmit={this.handleSubmit}
              render={({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
                isValid,
                setTouched
              }) => (
                <Container>
                  <Form onSubmit={handleSubmit} noValidate name="simpleForm">
                    <FormGroup>
                      <Label className="green-label" for="cpf">
                        Nome
                      </Label>
                      <Input
                        className="col-md-8"
                        invalid={touched.name && !!errors.name}
                        type="text"
                        name="name"
                        id="name"
                        onBlur={handleBlur}
                        value={this.state.modelo + " " + this.state.placa}
                      />
                      <FormFeedback>{errors.name}</FormFeedback>
                    </FormGroup>

                    <FormGroup>
                      <Label className="green-label" for="imei">
                        Identificador/IMEI
                      </Label>
                      <Input
                        className="col-md-3"
                        invalid={touched.imei && !!errors.imei}
                        type="text"
                        name="imei"
                        id="imei"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        mask="99999999999"
                        maskChar=" "
                        tag={InputMask}
                      />
                      <FormFeedback>{errors.imei}</FormFeedback>
                    </FormGroup>

                    <FormGroup>
                      <Label className="green-label" for="phone">
                        Telefone
                      </Label>
                      <Input
                        className="col-md-3"
                        invalid={touched.phone && !!errors.phone}
                        type="text"
                        name="phone"
                        id="phone"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.phone}
                        mask="(99) 99999-9999"
                        maskChar=" "
                        tag={InputMask}
                      />
                      <FormFeedback>{errors.phone}</FormFeedback>
                    </FormGroup>
                    
                    
                    <FormGroup>
                      <Label className="green-label" for="operator">
                        Operadora
                      </Label>
                      <Input
                        className="col-md-6"
                        invalid={touched.operator && !!errors.operator}
                        type="text"
                        name="operator"
                        id="operator"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.operator}
                      />
                      <FormFeedback>{errors.operator}</FormFeedback>
                    </FormGroup>

                    <FormGroup>
                      <Label className="green-label" for="cpf">
                        Modelo
                      </Label>
                      <Input
                        className="col-md-6"
                        invalid={touched.model && !!errors.model}
                        type="text"
                        name="model"
                        id="model"
                        onChange={e=>{handleChange(e); this.setState({modelo:e.target.value});}}
                        onBlur={handleBlur}
                        value={values.model}
                      />
                      <FormFeedback>{errors.model}</FormFeedback>
                    </FormGroup>

                    <FormGroup>
                      <Label className="green-label" for="cpf">
                        Placa
                      </Label>
                      <Input
                        className="col-md-3"
                        invalid={touched.placa && !!errors.placa}
                        type="text"
                        name="placa"
                        id="placa"
                        onChange={e=>{handleChange(e); this.setState({placa:e.target.value});}}
                        onBlur={handleBlur}
                        value={values.placa}
                        mask="aaa-9999"
                        maskChar=" "
                        tag={InputMask}
                      />
                      <FormFeedback>{errors.placa}</FormFeedback>
                    </FormGroup>

                    <FormGroup>
                      <Label className="green-label" for="contact">
                        Contato
                      </Label>
                      <Input
                        className="col-md-6"
                        invalid={touched.contact && !!errors.contact}
                        type="text"
                        name="contact"
                        id="contact"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.contact}
                      />
                      <FormFeedback>{errors.contact}</FormFeedback>
                    </FormGroup>

                    <Row form className="col-md-15">
                      <FormGroup className="col-md-3">
                        <Label className="green-label" for="category">
                          Categoria
                        </Label>
                        <Select
                          name="form-field-name2"
                          value={this.state.category}
                          options={categories2}
                          onChange={category => {
                            errors.category = "";
                            if (category == null) {
                              category = "";
                            }
                            this.setState({ category });
                          }}
                          placeholder="Selecione"
                        />

                        <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                          {errors.category}
                        </Label>
                      </FormGroup>

                      <FormGroup className="col-md-3">
                        <Label className="green-label" for="group">
                          Grupo
                        </Label>
                        <Select
                          name="form-field-name2"
                          value={this.state.group}
                          options={this.state.groups}
                          onChange={group => {
                            errors.group = "";
                            if (group == null) {
                              group = "";
                            }
                            this.setState({ group });
                          }}
                          placeholder="Selecione"
                        />

                        <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                          {errors.group}
                        </Label>
                      </FormGroup>

                      <FormGroup className="col-md-3">
                        <Label className="green-label" for="fuel">
                          Combustivel
                        </Label>
                        <Select
                          name="form-field-name2"
                          value={this.state.fuel}
                          options={fuels2}
                          onChange={fuel => {
                            errors.fuel = "";
                            if (fuel == null) {
                              fuel = "";
                            }
                            this.setState({ fuel });
                          }}
                          placeholder="Selecione"
                        />

                        <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                          {errors.fuel}
                        </Label>
                      </FormGroup>
                    </Row>

                    <FormGroup className="col-md-3">
                      <Label className="green-label" for="cpf">
                        Autonomia
                      </Label>
                      <InputGroup>
                        <Input
                          invalid={touched.autonomy && !!errors.autonomy}
                          type="text"
                          name="autonomy"
                          id="autonomy"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.autonomy}
                        />
                        <InputGroupAddon addonType="append">
                          Km/l
                        </InputGroupAddon>
                      </InputGroup>
                      <FormFeedback>{errors.autonomy}</FormFeedback>
                    </FormGroup>

                    <FormGroup className="col-md-3">
                      <Label className="green-label" for="cpf">
                        Limite de Velocidade
                      </Label>
                      <InputGroup>
                        <Input
                          invalid={touched.speed && !!errors.speed}
                          type="text"
                          name="speed"
                          id="speed"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.speed}
                        />
                        <InputGroupAddon addonType="append">
                          Km/h
                        </InputGroupAddon>
                      </InputGroup>
                      <FormFeedback>{errors.speed}</FormFeedback>
                    </FormGroup>

                    <Row className="justify-content-sm-start">
                      <Col sm="12" md={{ size: 6, offset: 3 }}>
                        <LaddaButton
                          type="submit"
                          className="col-6 btn btn-primary btn-ladda"
                          loading={isSubmitting}
                          onClick={e => {
                            handleSubmit(e);
                            this.toggleBtn("expLeft");
                          }}
                        >
                          Enviar
                        </LaddaButton>
                      </Col>
                    </Row>
                  </Form>
                </Container>
              )}
            />
          </CardBody>
        </Card>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session
});

const mapDispatchToProps = {};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Devices)
);
