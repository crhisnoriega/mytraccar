import React, { Component } from "react";

import { CardBody, Card, Button, Row, Col } from "reactstrap";

import Loading from "react-loading-bar";
import "react-loading-bar/dist/index.css";

import Iframe from "react-iframe";

class Map extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: true
    };
  }

  componentDidMount() {
    this.setState({ show: true });
  }

  onShow = () => {
    this.setState({ show: true });
  };

  onHide = () => {
    this.setState({ show: false });
  };

  render() {
    return (
      <Iframe
        className="mymap-size"
        width="100%"
        height="100%"
        url="http://35.237.5.236/mapa/mapa9.html"
        id="myId"
        display="block"
      />
    );
  }
}

export default Map;
