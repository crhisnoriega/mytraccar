import React, { Component } from "react";

import {
  InputGroupText,
  UncontrolledDropdown,
  Container,
  Col,
  CardBody,
  Card,
  Row,
  Button,
  FormGroup,
  Label,
  Input,
  FormFeedback,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  InputGroup,
  InputGroupAddon
} from "reactstrap";

import LaddaButton, {
  EXPAND_LEFT,
  EXPAND_RIGHT,
  EXPAND_UP,
  EXPAND_DOWN,
  CONTRACT,
  CONTRACT_OVERLAY,
  SLIDE_LEFT,
  SLIDE_RIGHT,
  SLIDE_UP,
  SLIDE_DOWN,
  ZOOM_IN,
  ZOOM_OUT
} from "react-ladda";

import "ladda/dist/ladda-themeless.min.css";

import { Formik, Form, ErrorMessage } from "formik";
import { Effect } from "formik-effect";
import * as Yup from "yup";

import { withRouter, Link } from "react-router-dom";
import { connect } from "react-redux";
import axios from "axios";
import base64 from "react-native-base64";

import InputMask from "react-input-mask";

// loading
import Loading from "react-loading-bar";
import "react-loading-bar/dist/index.css";

// select
import Select from "react-select";
import "react-select/dist/react-select.min.css";

class Users extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  toggleBtn = name => {
    this.setState({
      [name]: !this.state[name],
      progress: 0.5
    });
  };

  componentDidMount() {
    this.setState({ show: true });
    axios
      .post(
        "/api/getapicall",
        {
          url: "/groups",
          data: "",
          headers: {
            "Content-Type": "application/json",
            Authorization: "Basic " + this.props.session.token
          }
        },
        {
          headers: {
            "Content-Type": "application/json"
          }
        }
      )
      .then(res => {});
  }

  // validation
  validationSchema = function(values) {
    return Yup.object().shape({
      name: Yup.string().required("nome do usuario"),
      password: Yup.string().required("senha obrigatoria"),
      email: Yup.string().required("email obrigatorio"),
      phone: Yup.string().required("numero de telefone obrigatorio")
    });
  };

  myValidator = mySchema => {
    return values => {
      const validationSchema = mySchema(values);

      try {
        validationSchema.validateSync(values, { abortEarly: false });

        return {};
      } catch (error) {
        var err = this.extractError(error);
        return err;
      }
    };
  };

  extractError = validationError => {
    const FIRST_ERROR = 0;
    return validationError.inner.reduce((errors, error) => {
      return {
        ...errors,
        [error.path]: error.errors[FIRST_ERROR]
      };
    }, {});
  };

  handleSubmit = (values, { props = this.props, setSubmitting }) => {
    axios
      .post(
        "/api/apicall",
        {
          url: "/user",
          data: "",
          headers: {
            "Content-Type": "application/json",
            Authorization: "Basic " + this.props.session.token
          }
        },
        {
          headers: {
            "Content-Type": "application/json"
          }
        }
      )
      .then(res => {
        alert(JSON.stringify(res));
        setSubmitting(false);
      });

    return;
  };

  toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }

  selectItem = item => {
    alert(item);
    this.setState({
      selectItem: item
    });
  };

  saveChanges = value => {
    this.setState({ value });
  };

  render() {
    var fuels = ["Flex", "Gasolina", "Alcool", "Diesel", "GNV"];
    var fuels2 = fuels.map(item => {
      return { value: item, label: item };
    });

    return (
      <div>
        <Loading show={this.state.show} showSpinner={false} />
        <Card>
          <CardBody>
            <Formik
              initialValues={{
                name: "",
                password: "",
                phone: ""
              }}
              validate={this.myValidator(this.validationSchema)}
              onSubmit={this.handleSubmit}
              render={({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
                isValid,
                setTouched
              }) => (
                <Container>
                  <Form onSubmit={handleSubmit} noValidate name="simpleForm">
                    <FormGroup>
                      <Label className="green-label" for="cpf">
                        Nome
                      </Label>
                      <Input
                        className="col-md-8"
                        invalid={touched.name && !!errors.name}
                        type="text"
                        name="name"
                        id="name"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.name}
                      />
                      <FormFeedback>{errors.name}</FormFeedback>
                    </FormGroup>

                    <FormGroup>
                      <Label className="green-label" for="email">
                        Email
                      </Label>
                      <Input
                        className="col-md-4"
                        invalid={touched.email && !!errors.email}
                        type="text"
                        name="email"
                        id="email"
                        onChange={handleChange}
                        onBlur={handleBlur}                      
                      />
                      <FormFeedback>{errors.email}</FormFeedback>
                    </FormGroup>

                    <FormGroup>
                      <Label className="green-label" for="password">
                        Senha
                      </Label>
                      <Input
                        className="col-md-4"
                        invalid={touched.password && !!errors.password}
                        type="password"
                        name="password"
                        id="password"
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />
                      <FormFeedback>{errors.password}</FormFeedback>
                    </FormGroup>

                    <FormGroup>
                      <Label className="green-label" for="phone">
                        Telefone
                      </Label>
                      <Input
                        className="col-md-3"
                        invalid={touched.phone && !!errors.phone}
                        type="text"
                        name="phone"
                        id="phone"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.phone}
                        mask="(99) 99999-9999"
                        maskChar=" "
                        tag={InputMask}
                      />
                      <FormFeedback>{errors.phone}</FormFeedback>
                    </FormGroup>

                    <Row form className="col-md-15">
                      <FormGroup className="col-md-3">
                        <Label className="green-label" for="readonly">
                          Somente Leitura
                        </Label>
                        <Select
                          name="form-field-name2"
                          value={this.state.readonly}
                          options={[
                            { value: true, label: "Sim" },
                            { value: false, label: "Nao" }
                          ]}
                          onChange={readonly => {
                            this.setState({ readonly });
                          }}
                          placeholder="Selecione"
                        />

                        <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                          {errors.readonly}
                        </Label>
                      </FormGroup>

                      <FormGroup className="col-md-3">
                        <Label className="green-label" for="distance">
                          Distancia
                        </Label>
                        <Select
                          name="form-field-name2"
                          value={this.state.distance}
                          options={[{ value: "km", label: "Kilometros" }]}
                          onChange={distance => {
                            errors.distance = "";
                            if (distance == null) {
                              distance = "";
                            }
                            this.setState({ distance });
                          }}
                          placeholder="Selecione"
                        />

                        <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                          {errors.distance}
                        </Label>
                      </FormGroup>
                    </Row>

                    <Row form className="col-md-15">
                      <FormGroup className="col-md-3">
                        <Label className="green-label" for="speed">
                          Velocidade
                        </Label>
                        <Select
                          name="form-field-name2"
                          value={this.state.speed}
                          options={[{ value: "KM/h" }]}
                          onChange={speed => {
                            errors.speed = "";
                            if (speed == null) {
                              speed = "";
                            }
                            this.setState({ speed });
                          }}
                          placeholder="Selecione"
                        />

                        <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                          {errors.speed}
                        </Label>
                      </FormGroup>

                      <FormGroup className="col-md-3">
                        <Label className="green-label" for="disable">
                          Desativado
                        </Label>
                        <Select
                          name="form-field-name2"
                          value={this.state.disable}
                          options={[
                            { value: true, label: "Sim" },
                            { value: false, label: "Nao" }
                          ]}
                          onChange={disable => {
                            this.setState({ disable });
                          }}
                          placeholder="Selecione"
                        />

                        <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                          {errors.disable}
                        </Label>
                      </FormGroup>
                    </Row>

                    <Row form className="col-md-15">
                      <FormGroup className="col-md-3">
                        <Label className="green-label" for="admin">
                          Administrador
                        </Label>
                        <Select
                          name="form-field-name2"
                          value={this.state.admin}
                          options={[{ value: "KM/h" }]}
                          onChange={admin => {
                            errors.admin = "";
                            if (admin == null) {
                              admin = "";
                            }
                            this.setState({ admin });
                          }}
                          placeholder="Selecione"
                        />

                        <Label style={{ color: "#f63c3a", fontSize: "80%" }}>
                          {errors.admin}
                        </Label>
                      </FormGroup>

                    </Row>


                    <FormGroup className="col-md-3">
                      <Label className="green-label" for="cpf">
                        Autonomia
                      </Label>
                      <InputGroup>
                        <Input
                          invalid={touched.autonomy && !!errors.autonomy}
                          type="text"
                          name="autonomy"
                          id="autonomy"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.autonomy}
                        />
                        <InputGroupAddon addonType="append">
                          Km/l
                        </InputGroupAddon>
                      </InputGroup>
                      <FormFeedback>{errors.autonomy}</FormFeedback>
                    </FormGroup>

                    <FormGroup className="col-md-3">
                      <Label className="green-label" for="cpf">
                        Limite de Velocidade
                      </Label>
                      <InputGroup>
                        <Input
                          invalid={touched.speed && !!errors.speed}
                          type="text"
                          name="speed"
                          id="speed"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.speed}
                        />
                        <InputGroupAddon addonType="append">
                          Km/h
                        </InputGroupAddon>
                      </InputGroup>
                      <FormFeedback>{errors.speed}</FormFeedback>
                    </FormGroup>

                    <Row className="justify-content-sm-start">
                      <Col sm="12" md={{ size: 6, offset: 3 }}>
                        <LaddaButton
                          type="submit"
                          className="col-6 btn btn-primary btn-ladda"
                          loading={isSubmitting}
                          onClick={e => {
                            handleSubmit(e);
                            this.toggleBtn("expLeft");
                          }}
                        >
                          Enviar
                        </LaddaButton>
                      </Col>
                    </Row>
                  </Form>
                </Container>
              )}
            />
          </CardBody>
        </Card>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session
});

const mapDispatchToProps = {};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Users)
);
