import React, { Component } from "react";
import {
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Table,
  DropdownMenu,
  DropdownToggle,
  Dropdown,
  DropdownItem,
  Button,
  Card,
  CardHeader,
  CardBody,
  Col,
  CustomInput,
  Form,
  FormFeedback,
  FormGroup,
  Label,
  Input,
  Row
} from "reactstrap";
import { Formik, Field } from "formik";
import * as Yup from "yup";

import axios, { post } from "axios";

import { connect } from "react-redux";
import { doLogin, setInvestidor } from "../redux/redux";
import history from "../history/history";

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import InputMask from "react-input-mask";

import UserRow from "./UserRow";
import "spinkit/css/spinkit.css";

class UsersList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      filter: "",
      show: false,
      users: []
    };
  }

  searchUser(filter) {
    this.setState({ show: true });
    axios
      .post(
        "/api/getapicall",
        {
          url: "/users",
          data: "",
          headers: {
            "Content-Type": "application/json",
            Authorization: "Basic " + this.props.session.token
          }
        },
        {
          headers: {
            "Content-Type": "application/json"
          }
        }
      )
      .then(res => {
        this.setState({
          users: res.data,
          show: false
        });
      });
  }

  componentDidMount() {}

  onChange = e => {
    var filter = e.target.value;
    if (filter.length >= 3) {
      this.searchUser(filter);
    } else {
      this.setState({ users: [] });
    }
  };

  callback = () => {
    this.componentDidMount();
  };

  render() {
    let userLines = this.state.users.map(user => {
      return <UserRow callback={this.callback} user={user} />;
    });

    return (
      <div className="animated fadeIn">
        <InputGroup style={{ width: "50%" }} className="mb-4">
          <InputGroupAddon addonType="prepend">
            <InputGroupText>
              <i className="icon-user" />
            </InputGroupText>
          </InputGroupAddon>
          <Input
            name="search"
            onChange={this.onChange}
            autoFocus={true}
            type="text"
            placeholder="nome, email"
            autoComplete="search"
          />
        </InputGroup>
        {this.state.show ? (
          <div className="sk-three-bounce">
            <div className="sk-child sk-bounce1" />
            <div className="sk-child sk-bounce2" />
            <div className="sk-child sk-bounce3" />
          </div>
        ) : null}
        {this.state.users.length != 0 && !this.state.show ? (
          <Row>
            <Col>
              <Card>
                <CardBody>
                  <Table noborder hover responsive size="sm">
                    <thead className="thead-light">
                      <tr>
                        <th className="text-center">
                          <i className="icon-user" />
                        </th>
                        <th>Nome</th>
                        <th>Email</th>
                        <th>Telefone</th>
                        <th>Admin</th>
                      </tr>
                    </thead>
                    <tbody>{userLines}</tbody>
                  </Table>
                </CardBody>
              </Card>
            </Col>
          </Row>
        ) : (
          <Row>
            <Col>
              <CardBody />
            </Col>
          </Row>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session
});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UsersList);
