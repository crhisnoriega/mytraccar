export default {
  items: [
    {
      name: "Dashboard",
      url: "/app/dashboard",
      icon: "icon-chart",
      badge: {
        variant: "info",
        text: "NEW"
      }
    },

    {
      name: "Mapa",
      url: "/app/map",
      icon: "icon-map",
      badge: {
        variant: "info",
        text: "NEW"
      }
    },
    {
      name: "Dispositivos",
      url: "/devices",
      icon: "icon-feed",
      children: [
        {
          name: "Registrar",
          url: "/app/devices/register",
          icon: "icon-note",
          badge: {
            variant: "info",
            text: "NEW"
          }
        },
        {
          name: "Lista",
          url: "/app/devices/list",
          icon: "icon-list",
          badge: {
            variant: "info",
            text: "NEW"
          }
        }
      ]
    },

    {
      name: "Usuarios",
      url: "/users",
      icon: "icon-user",
      children: [
       
        {
          name: "Cadastro",
          url: "/app/users/register",
          icon: "icon-note"
         
        },
        {
          name: "Busca",
          url: "/app/users/list",
          icon: "icon-list"
         
        }
      ]
    },
    {
      name: "Permissões",
      url: "/app/permissions",
      icon: "icon-lock",
      badge: {
        variant: "info",
        text: "NEW"
      }
    }
  ]
};
