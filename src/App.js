import React, { Component } from 'react';
import { BrowserRouter, HashRouter, Route, Switch, Router } from 'react-router-dom';
import './App.css';
// Styles
// CoreUI Icons Set
import '@coreui/icons/css/coreui-icons.min.css';
// Import Flag Icons Set
import 'flag-icon-css/css/flag-icon.min.css';
// Import Font Awesome Icons Set
import 'font-awesome/css/font-awesome.min.css';
// Import Simple Line Icons Set
import 'simple-line-icons/css/simple-line-icons.css';
// Import Main styles for this application
import './scss/style.css'

// Containers
import { DefaultLayout } from './containers';

import Login from './pages/Login';
import Hello from './pages/Hello';

import history from './history/history'
import { Provider } from 'react-redux';
import { store } from './redux/redux';

// import { renderRoutes } from 'react-router-config';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router history={history}>
          <Switch>
            <Route path="/login" name="Login Page" exact={true} component={Login} />
            <Route path="/app" name="Home" component={DefaultLayout} />         
          </Switch>
        </Router>
      </Provider>
    );
  }
}

export default App;
