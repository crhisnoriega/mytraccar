import React from "react";
import Loadable from "react-loadable";

function Loading() {
  return <div>Loading...</div>;
}

const Hello = Loadable({
  loader: () => import("./pages/Hello"),
  loading: Loading
});

const Dashboard = Loadable({
  loader: () => import("./views/Dashboard"),
  loading: Loading
});

const Devices = Loadable({
  loader: () => import("./views/Devices"),
  loading: Loading
});

const DevicesList = Loadable({
  loader: () => import("./views/DevicesList"),
  loading: Loading
});

const UsersList = Loadable({
  loader: () => import("./views/UsersList"),
  loading: Loading
});

const Users = Loadable({
  loader: () => import("./views/Users"),
  loading: Loading
});

const Map = Loadable({
  loader: () => import("./views/Map"),
  loading: Loading
});

const UserPermissions = Loadable({
  loader: () => import("./views/UserPermissions"),
  loading: Loading
});

const routes = [
  {
    path: "/app/dashboard",
    name: "Dashboard",
    component: Dashboard,
    exact: true
  },

  { path: "/app/map", name: "Mapa", component: Map, exact: true },
  {
    path: "/app/devices/list",
    name: "Dispositivos",
    component: DevicesList,
    exact: true
  },
  {
    path: "/app/devices/register",
    name: "Registro de Dispositivo",
    component: Devices,
    exact: true
  },
  {
    path: "/app/users/list",
    name: "Usuarios",
    component: UsersList,
    exact: true
  },
  {
    path: "/app/users/register",
    name: "Registrar Usuario",
    component: Users,
    exact: true
  },
  { path: "/app/permissions",
    name: "Permissões de Usuario",
    component:UserPermissions,
    exact: true
  }
];

export default routes;
