import { withRouter, Link } from "react-router-dom";
import React, { Component } from "react";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Media,
  Button,
  Card,
  CardBody,
  CardGroup,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row
} from "reactstrap";

import axios from "axios";
import history from "../history/history";

import { connect } from "react-redux";
import { activateGeod, closeGeod, doLogin } from "../redux/redux";

import StringMask from "string-mask";
import InputMask from "react-input-mask";

class Hello extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: "",
      password: "",
      validate: {
        invaliduser: false,
        invalidpassword: false
      },
      showerror: false,
      message: ""
    };
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleInputChange(e) {
    console.log(this.state);
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  routeChange(e) {
    e.preventDefault();
    console.log(this.state.user);
    console.log(this.state.password);

    let login = this.state.user.replace(/\./g, "").replace(/\-/g, "");

    axios
      .post(
        "/api/login",
        {
          login: login,
          password: this.state.password
        },
        {
          headers: {
            "Content-Type": "application/json"
          }
        }
      )
      .then(res => {
        if (res.data == null) {
          this.setState({ showerror: true, message: "Usuário não existe" });
          return;
        }

        if (res.data.status != "aprove") {
          this.setState({ showerror: true, message: "Usuário não aprovado" });
          return;
        }

        if (res.data != null && res.data.password == this.state.password) {
          this.props.doLogin(res.data);
          history.push("/app");
        } else {
          this.setState({
            showerror: true,
            message: "Usuário ou senha inválida"
          });
        }
      })
      .catch(error => {
        alert(JSON.stringify(error));
        this.setState({ showerrornetwork: true });
      });
  }

  handleClose(e) {
    this.setState({ showerror: false, showerrornetwork: false });
  }

  render() {
    return (
      <div className="login_app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <h2>Hello</h2>
          </Row>
        </Container>

        <Modal
          style={{ marginTop: "20%" }}
          isOpen={this.state.showerror}
          toggle={this.toggleInfo}
          className={"modal-danger"}
        >
          <ModalHeader toggle={this.toggleInfo}>
            Erro de Autenticação
          </ModalHeader>
          <ModalBody>{this.state.message}</ModalBody>
          <ModalFooter>
            <Button color="danger" onClick={e => this.handleClose(e)}>
              Fechar
            </Button>{" "}
          </ModalFooter>
        </Modal>

        <Modal
          style={{ marginTop: "20%" }}
          isOpen={this.state.showerrornetwork}
          toggle={this.toggleInfo}
          className={"modal-danger"}
        >
          <ModalHeader toggle={this.toggleInfo}>
            Erro de Autenticação
          </ModalHeader>
          <ModalBody>Erro inesperado</ModalBody>
          <ModalFooter>
            <Button color="danger" onClick={e => this.handleClose(e)}>
              Fechar
            </Button>{" "}
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session
  // geod: state.geod,
});

const mapDispatchToProps = {
  activateGeod,
  closeGeod,
  doLogin
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Hello)
);
