import { withRouter, Link } from "react-router-dom";
import React, { Component } from "react";
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Media,
  Button,
  Card,
  CardBody,
  CardGroup,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row
}
from "reactstrap";

import axios from "axios";
import history from "../history/history";
import base64 from "react-native-base64";

import logo from "../assets/img/brand/logo_prime.png";

import { connect } from "react-redux";
import { activateGeod, closeGeod, doLogin, setToken } from "../redux/redux";

import StringMask from "string-mask";
import InputMask from "react-input-mask";

import scriptLoader from 'react-async-script-loader'


class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: "",
      password: "",
      validate: {
        invaliduser: false,
        invalidpassword: false
      },
      showerror: false,
      message: ""
    };
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleInputChange(e) {
    console.log(this.state);
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  loadJS = function(src) {
    var tag = document.createElement('script');
    tag.async = false;
    tag.type = 'text/javascript';
    tag.src = src;
    document.getElementsByTagName('body')[0].appendChild(tag);


  }

  loadCSS = function(src) {
    var tag = document.createElement('script');
    tag.async = false;
    tag.type = 'text/javascript';
    tag.src = src;
    document.getElementsByTagName('body')[0].appendChild(tag);


  }

  componentWillReceiveProps({ isScriptLoaded, isScriptLoadSucceed }) {

  }

  componentDidMount() {

  }


  routeChange(e) {
    e.preventDefault();
    console.log(this.state.user);
    console.log(this.state.password);

    let login = this.state.user.replace(/\./g, "").replace(/\-/g, "");

    axios
      .post(
        "/api/apicall", {
          url: "/session",
          data: "email=" + this.state.user + "&password=" + this.state.password,
          headers: {
            "Content-Type": "application/x-www-form-urlencoded"
          }
        }, {
          headers: {
            "Content-Type": "application/json"
          }
        }
      )
      .then(res => {
        if (res.data.error != null) {
          this.setState({ showerror: true, message: res.data.error });
          return;
        }

        if (res.data.id != null) {
          this.props.doLogin(res.data);
          this.props.setToken(
            base64.encode(this.state.user + ":" + this.state.password)
          );
          history.push("/app");
          return;
        }
      });
  }

  handleClose(e) {
    this.setState({ showerror: false, showerrornetwork: false });
  }

  render() {
    return (
      <div id="login_body" className="mybody2">
      
       
       
                    <Form  className="mylogin form" onSubmit={e => this.routeChange(e)}>
                      <Row>
                        <Media
                          style={{
                            display: "table",
                            margin: "0 auto",
                            marginBottom: "30px"
                          }}
                          object
                          src={logo}
                        />
                      </Row>

                      <p
                        style={{
                          display: "table",
                          margin: "0 auto",
                          marginBottom: "20px",
                          color: "white"
                        }}
                        className="login-text-muted"
                      >
                        Entre na sua conta
                      </p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText
                          className="label-text">
                            <i className="icon-user" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          className="input-text"
                          name="user"
                          type="text"
                          placeholder="Usuario"
                          autoComplete="username"
                          value={this.state.user}
                          onChange={e => {
                            this.handleInputChange(e);
                          }}
                          invalid={this.state.validate.invaliduser}
                        />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText  className="label-text">
                            <i className="icon-lock" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                         className="input-text"
                          name="password"
                          type="password"
                          placeholder="Senha"
                          autoComplete="current-password"
                          value={this.state.password}
                          onChange={e => {
                            this.handleInputChange(e);
                          }}
                          invalid={this.state.validate.invalidpassword}
                        />
                      </InputGroup>
                      <Button
                        style={{ display: "table", width: "100%" }}
                        color="primary"
                        className="px-4"
                      >
                        Entrar 
                        <i className="fa fa-sign-in" />
                      </Button>
                    </Form>
                  
       
       

        <Modal
          style={{ marginTop: "20%" }}
          isOpen={this.state.showerror}
          toggle={this.toggleInfo}
          className={"modal-danger"}
        >
          <ModalHeader toggle={this.toggleInfo}>
            Erro de Autenticação
          </ModalHeader>
          <ModalBody>{this.state.message}</ModalBody>
          <ModalFooter>
            <Button color="danger" onClick={e => this.handleClose(e)}>
              Fechar
            </Button>{" "}
          </ModalFooter>
        </Modal>

        <Modal
          style={{ marginTop: "20%" }}
          isOpen={this.state.showerrornetwork}
          toggle={this.toggleInfo}
          className={"modal-danger"}
        >
          <ModalHeader toggle={this.toggleInfo}>
            Erro de Autenticação
          </ModalHeader>
          <ModalBody>Erro inesperado</ModalBody>
          <ModalFooter>
            <Button color="danger" onClick={e => this.handleClose(e)}>
              Fechar
            </Button>{" "}
          </ModalFooter>
        </Modal>
     
      </div>
    );
  }
}

const mapStateToProps = state => ({
  session: state.session
});

const mapDispatchToProps = {
  activateGeod,
  closeGeod,
  doLogin,
  setToken
};




export default scriptLoader(
    ["https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.min.js",
      
      "/vegas/dist/vegas.js"
    ])
  (withRouter(
    connect(
      mapStateToProps,
      mapDispatchToProps
    )(Login)
  ));
